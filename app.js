require('dotenv').config();
require('newrelic');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jsonwebtoken');
const MongoDBStore = require('connect-mongodb-session')(session);

const Usuario = require('./models/usuario');
const Token = require('./models/token');

var indexRouter = require('./routes/index');
var usuarioRouter = require('./routes/usuario');
var tokenRouter = require('./routes/token');
var bicicletaRouter = require('./routes/bicicleta');
var authAPIRouter = require('./routes/api/auth');
var bicicletaAPIRouter = require('./routes/api/bicicleta');
var usuarioAPIRouter = require('./routes/api/usuario');

let store;
if (process.env.NODE_ENV === 'development') {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function (error) {
    assert.ifError(error);
    assert.ok(false);
  });
}

let app = express();
app.set('secretKey', 'red_biciclita_!!!!!.**1121324');
app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_biciclita_!!!!!.**'
}));

//Conect DB
var mongoose = require('mongoose');
const { collection } = require('./models/token');
const { assert } = require('console');
//Desarrollo
//var mongoDB = 'mongodb://localhost:27017/red_bicicleta'
//Produccion
//mongodb+srv://admin:<password>@red-bicicleta.tahha.mongodb.net/<dbname>?retryWrites=true&w=majority

var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

//Login
app.get('/login', function (req, res) {
  res.render('session/login');
});

app.post('/login', function (req, res, next) {
  passport.authenticate('local', function (err, usuario, info) {
    if (err) return next(err);
    if (!usuario) return res.render('session/login', { info });
    req.logIn(usuario, function (err) {
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function (req, res) {
  req.logOut();
  res.redirect('/')
});

app.get('/forgotPassword', function (req, res) {
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function (req, res) {
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    if (usuario) return res.render('session/forgotPassword', { info: { message: 'No existe email para un usuario existente.' } });

    usuario.resetPassword(function (err) {
      if (err) return next(err);
      console.log('session/forgotPasswordMessage');
    });

    res.render('session/forgotPasswordMessage');
  });
});

app.get('resetPassword/:token', function (req, res, next) {
  Token.findOne({ token: req.body.token }, function (err, token) {
    if (!token) return res.status(400).send({ type: 'not-verified', msg: 'No exite usuario asociado al token. Verifique que su token no haya expirado.' });

    Usuario.findById(token._userId, function (err, usuario) {
      if (!usuario) return res.status(400).send({ msg: 'No existe usuario asociado al token.' });
      res.render('session/resetPassword', { errors: {}, usuario: usuario });
    });
  });
});

app.post('/resetPassword', function (req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render('session/resetPassword', { errors: { confrim_password: { message: 'No coninciden con el password ingresado' } }, usuario: new Usuario({ email: req.body.email }) });
  }
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    usuario.password = req.body.password;
    usuario.save(function (err) {
      if (err) {
        res.render('session/resetPassword', { errors: err.errors, usuario: new Usuario({ email: req.body.email }) });
      } else {
        res.redirect('/login');
      }
    });
  });
});



app.use('/', indexRouter);
app.use('/usuario', usuarioRouter);
app.use('/token', tokenRouter);
app.use('/bicicleta', loggedIn, bicicletaRouter);
app.use('/api/auth', authAPIRouter);
app.use('/api/bicicleta', validarUsuario, bicicletaAPIRouter);
app.use('/api/usuario', usuarioAPIRouter);
app.use('/privacy_policy', function (req, res) {
  res.sendFile('public/policy_privacy.html');
});
app.use('/google3e0b0bb54883990d', function (req, res) {
  res.sendFile('public/google3e0b0bb54883990d.html');
});
app.get('/auth/google',
  passport.authenticate('google', {
    scope: [
      'https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/plus.profile.emails.read']
  })
);
app.get('/auth/google/callback',
  passport.authenticate('google', { 
    successRedirect: '/',
    failureRedirect: '/error' })
);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log('Usuario sin loguearse.');
    res.redirect('/login');
  }
};

function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
    if (err) {
      res.json({ status: "error", message: err.message, data: null });
    } else {
      req.body.userId = decoded.id;
      console.log('jwt verify: ' + decoded);
      next();
    }
  });
};

module.exports = app;
