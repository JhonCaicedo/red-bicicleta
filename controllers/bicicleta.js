var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (err, targetBicis) {
        if (err) console.log(err);
        res.render('bicicleta/index', {bicis : targetBicis});
    })
}

exports.bicicleta_create_get = function (req, res){
    res.render('bicicleta/create');
}

exports.bicicleta_create_post = function (req, res){
    var ubicacion = [req.body.lat, req.body.lng];
    var bici = new Bicicleta({ code: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: ubicacion});
    Bicicleta.add(bici, function (err, newBici) {
        if (err) console.log(err);
    });

    res.redirect('/bicicleta');
}

exports.bicicleta_update_get = function (req, res){
    var bici = Bicicleta.findBycode(req.params.code);
    res.render('bicicleta/update', {bici});
}

exports.bicicleta_update_post = function (req, res){
    var bici = Bicicleta.findBycode(req.params.code);
    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    
    res.redirect('/bicicleta');
}

exports.bicicleta_view_get = function (req, res){
    var bici = Bicicleta.findBycode(req.params.code);
    res.render('bicicleta/view', {bici});
}

exports.bicicleta_view_post = function (req, res){
    var bici = Bicicleta.findBycode(req.params.code);
    res.redirect('/bicicleta');
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.removeBycode(req.body.code);

    res.redirect('/bicicleta');
}
