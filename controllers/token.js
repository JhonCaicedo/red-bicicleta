var Usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = {
    confirmationGet: function(req, res, next){
        Token.findOne({ token: req.params.token}, function(err, token){
            if(!token) return res.status(400).send({type: 'not-verified', msg: 'No encontramos usuario con este token: el token a expirado.'});
            Usuario.findById(token._userId, function(err, usuario){
                if(!usuario) return res.status(400).send({msg: 'No encontramos el usaurio con este token: indicado.'});
                if(usuario.verificado) return res.redirect('/usuarios');
                usuario.verificado = true;
                usuario.save(function(err){
                    if(err) return res.status(500).send({ msg: err.message});
                    res.redirect('/');
                });
            });
        });
    },
}