var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicleta', function () {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('Se a conectado para test en base de datos!');
            done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agregar solo una bici', (done) => {
            var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            Bicicleta.add(aBici, function (err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.finByCode', () => {
        it('debe devover la bici code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                if (err) console.log(err);

                var aBici2 = new Bicicleta({ code: 1, color: "rojo", modelo: "montaña" });
                Bicicleta.add(aBici2, function (err, newBici) {
                    if (err) console.log(err);
                    Bicicleta.findByCode(1, function (err, targetBici) {
                        expect(targetBici.code).toBe(aBici2.code);
                        expect(targetBici.color).toBe(aBici2.color);
                        expect(targetBici.modelo).toBe(aBici2.modelo);

                        done();
                    });
                });
            });
        });
    });

    describe('Bicicleta.deleteByCode', () => {
        it('debe eliminar la bici code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                if (err) console.log(err);

                var aBici2 = new Bicicleta({ code: 1, color: "rojo", modelo: "montaña" });
                var aBici3 = new Bicicleta({ code: 2, color: "verde", modelo: "murbana" });

                Bicicleta.add(aBici2, function (err, newBici) {
                    if (err) console.log(err);
                    Bicicleta.add(aBici3, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (err, targetBici) {
                            if (err) console.log(err);
                            expect(targetBici.code).toBe(aBici2.code);
                            expect(targetBici.color).toBe(aBici2.color);
                            expect(targetBici.modelo).toBe(aBici2.modelo);
                            Bicicleta.removeByCode(1, function (err, targetBici) {
                                if (err) console.log(err);
                                Bicicleta.allBicis(function (err, bicis) {
                                    expect(bicis.length).toBe(1);
                                    done();
                                });
                            });
                        });
                    });
                });
                
                
                
            });
        });
    });
});
