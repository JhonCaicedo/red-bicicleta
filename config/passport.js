const passport = require('passport');
const Usuario = require('../models/usuario');
const LocalStrategy = require('passport-local').Strategy;
const GoogelStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook-token');

passport.use(new FacebookStrategy({
    clientID: process.env.FACEBOOK_APP_ID,
    clientSecret: process.env.FACEBOOK_APP_SECRET,
},
    function (accessToken, refreshToken, profile, done) {
        try{
            User.findOneOrCreateByFacebook(profile, function (err, user) {
                if(err) console.log('err' + err);
                return done(err, user);
            });
        } catch(err2){
            console.log(err2);
            return done(err2, null);
        }
    }
));

passport.use(new LocalStrategy(
    function (email, password, done) {
        Usuario.findOne({ email: email }, function (err, usuario) {
            if (err) return done(null);
            if (!usuario) return done(null, false, { message: 'Email no existe o esta mal escrito.' });
            if (!usuario.validPassword(password)) return done(null, false, { message: 'El password ingresado no es valido.' });

            return done(null, usuario);
        });
    }
));

passport.use(new GoogelStrategy({
    clientID: process.env.GOOGLE_ID_CLIENT,
    clientSecret: process.env.GOOGLE_SECRET_CLIENT,
    callbackURL: process.env.HOST + "/auth/google/callbachk"
},
    function (accessToken, refreshToken, profile, cb) {
        console.log(profile);

        Usuario.findOneAndCreateByGoogle(profile, function (err, user) {
            return cb(err, user);
        });
    })
);

passport.serializeUser(function (user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
    Usuario.findById(id, function (err, usuario) {
        cb(err, usuario);
    });
});

module.exports = passport;