var map_one = L.map('map').setView([3.4142326,-76.5372511], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map_one);

$.ajax({
    dataType: "json",
    url: "/api/bicicleta/find",
    data: { id: 1 },
    success: function(result){
        console.log(result);
        result.Bicicleta = function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map_one);
        };
    }
});